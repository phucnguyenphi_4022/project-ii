<?php

namespace App\Http\Controllers\Employee;

use App\Http\Controllers\Controller;
use App\Models\Salary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SalaryController extends Controller
{
    public function list_salaries($start_month=null, $end_month=null){
        $employee_id = Auth::user()->employee()->first()->id;
        if ($start_month and $end_month) {
//            Raw select eloquent
//            $start_month = Carbon::parse($start_month.' 00:00:00')->format("Y-m-d H:i:s");
//            $end_month = Carbon::parse($end_month.' 23:59:59')->format("Y-m-d H:i:s");
//            $attendances = DB::select('
//                select * from attendances
//                where created_at >= ? and created_at <= ?
//                order by id desc
//               ', [$start_month, $end_month]);
            $salaries = Salary::with('info_detail')->with('employee')->where('employee_id', $employee_id)->where('month', '>=', $start_month)->where('month', '<=', $end_month)->orderby('month', 'desc')->get();
            $get_all = false;
        } else {
            $salaries = Salary::with('info_detail')->with('employee')->where('employee_id', $employee_id)->orderBy('month', 'desc')->get();
            $get_all = true;
        }

        $data = [
            'get_all' => $get_all,
            'start_month' => Carbon::parse($start_month)->format("m-Y"),
            'end_month' => Carbon::parse($end_month)->format("m-Y"),
            'salaries' => $salaries
        ];

        return view('employee.salary.list_salary')->with($data);
    }

    public function list_salaries_range(Request  $request){
        $current_month = Carbon::now()->format('Y-m');
        $validator = Validator::make($request->all(), [
            'start_month' => ['required', function ($attribute, $value, $fail) use ($current_month){
                if ($value > $current_month){
                    $fail('The '.$attribute.' must be before or equal this month');
                }
            }],
            'end_month' => ['required', 'after_Or_Equal:start_month', function ($attribute, $value, $fail) use ($current_month){
                if ($value > $current_month){
                    $fail('The '.$attribute.' must be before or equal this month');
                }
            }],
        ]);
        if ($validator->fails())
            return back()->withErrors($validator);

        return self::list_salaries(request('start_month'), request('end_month'));
    }
}
